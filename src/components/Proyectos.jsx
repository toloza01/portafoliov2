
import React,{useState,useEffect} from "react";
import teloregalo from "../assets/img/teloregalo.png";


const Proyecto = () => {
  const proyectos = [
    {
      nombre: "Te lo regalo",
      descripcion: "Sitio web para regalar articulos que lo necesiten para algun proyecto.",
      img: teloregalo,
    },
    {
      nombre: "Tolowebs",
      descripcion: "Mi sitio web para ofrecer mis servicios de desarrollador web.",
      img: teloregalo,
    },
    {
      nombre: "Mi tienda",
      descripcion: "Tienda online para multipropositos",
      img: teloregalo,
    },
    {
      nombre: "Mi tienda",
      descripcion: "Tienda online para multipropositos",
      img: teloregalo,
    },
    {
      nombre: "Mi tienda",
      descripcion: "Tienda online para multipropositos",
      img: teloregalo,
    },
    {
      nombre: "Mi tienda",
      descripcion: "Tienda online para multipropositos",
      img: teloregalo,
    },
    {
      nombre: "Mi tienda",
      descripcion: "Tienda online para multipropositos",
      img: teloregalo,
    },
    
  ];

  const [busqueda, setbusqueda] = useState("");
  const [listaproyectos, setproyectos] = useState(proyectos);


  useEffect(() => {
      
    setproyectos(listaproyectos)
      
  });

 const buscar = (query) =>{
     console.log(query);
     var resultado = [];
     listaproyectos.forEach((proyecto)=>{
        if(proyecto.nombre.toLowerCase().indexOf(query)!=-1){
          resultado.push(proyecto);
        }else{
           
        }
     });
     if(resultado.length == 0){
       //console.log("No hay resultados!!")
     }
     setbusqueda(query);
     setproyectos(resultado);
 }
 const EstadoInicial = ()=>{
     setproyectos(proyectos)
     setbusqueda("")
 }


  return (
    <div className="proyectos_main">
      <div className="proyectos__main_header">
        <div className="header_title">
          <h1>Conoce mis proyectos!</h1>
        </div>
        <div className="header_subtitle">
          <h4>
            Estos son mis mejores proyectos y de los que mas me enorgullezco.
          </h4>
        </div>
      </div>
      <div className="proyectos__buscador">
        <input
          type="text"
          placeholder="Busca algunos de mis proyectos..."
          className="buscador"
          onSubmit={ e=>buscar(e.target.value) }
          value = {busqueda}
        />
        <button>
          <i className="fa fa-search"></i>
        </button>
      </div>
      <h1>Tu busqueda: {busqueda}</h1>
      <div className="list-proyectos">
        <div className="container-proyectos">
          

          {listaproyectos.map((proyecto, index) => {
            return (
              <div key={index} className="card">
                <figure>
                  <img src={proyecto.img} alt="Te lo regalo" />
                </figure>
                <div className="contenido">
                  <h3>{proyecto.nombre}</h3>
                  <p>{proyecto.descripcion}</p>
                  <a href="#">Leer mas</a>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Proyecto;
