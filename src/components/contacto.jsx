import React from 'react';

const Contacto = () =>{




    
    return(
        <div className="contacto">
           <h1 className="logo">Contactame</h1>
           <div className="contact-wrapper">
               <div className="contact-form">
                 <h3>Contactame</h3>
                 <form action="">
                     <p>
                         <label htmlFor="nombre">Nombre Completo</label>
                         <input type="text" name="nombre"/>
                     </p>
                     <p>
                         <label htmlFor="email">Email</label>
                         <input type="email" name="email" id="email"/>
                     </p>
                     <p>
                         <label htmlFor="telefono">telefono: </label>
                         <input type="tel" name="telefono" id="" pattern="4 ^(+?56)?(\s?)(0?9)(\s?)[987654]\d{7}$"/>
                     </p>
                     <p>
                         <label htmlFor="Asunto">Asunto</label>
                         <input type="text" name="asunto"/>
                     </p>
                     <p className="block">
                         <label htmlFor="mensaje">Mensaje</label>
                         <textarea name="mensaje" id="mensaje" cols="30" rows="4"></textarea>
                     </p>
                     <p className="block">
                         <button type="submit">Enviar</button>
                         
                     </p>
                 </form>
               </div>
               <div className="contact-info">
                    <h4>Mas informacion</h4>
                    <ul>
                        <li>
                            <i className="fas fa-map-marker-alt"></i>
                            Arauco, Chile
                        </li>
                        <li>
                            <i className="fas fa-phone">
                                
                            </i>
                            (+56)9 64617091
                        </li>
                        <li>
                            <i className="far fa-envelope"> estebantoloza1998@gmail.com</i>
                        </li>
                    </ul>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut, delectus dignissimos est nisi error optio sapiente odio perspiciatis eaque iure quia repellendus, eveniet laboriosam asperiores placeat! Asperiores beatae error deserunt!
                    </p>
                    <p>Tolowebs.com</p>
               </div>
               
           </div>
        </div> 
    )
}

export default Contacto;